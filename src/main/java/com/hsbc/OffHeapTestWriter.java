package com.hsbc;

import com.google.common.base.Stopwatch;
import com.hsbc.PostalCodeRangeImpl;
import net.openhft.chronicle.map.ChronicleMap;
import net.openhft.chronicle.map.ChronicleMapBuilder;

import java.io.IOException;
import java.lang.instrument.Instrumentation;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import static java.util.concurrent.ThreadLocalRandom.*;

/**
 * @author kkulagin --> Dee
 * @since 09.11.2014
 */

public class OffHeapTestWriter {
	
	private static final int SHOW_INTERVAL_ITERATIONS = 100_000;
	private static final int ENTRIES = 5_000_000;

	public static void main(String[] args) throws IOException, InterruptedException {
		option3();
	}

	private static void option3() throws IOException, InterruptedException {
		Path file = getFile();
		// Files.deleteIfExists(file);
		// int entries = 500_000_000;
		
		
		// wrong size (smaller) but CM doesn't show error ! the actually map put into file is less than defined ENTRIES
//		int valueSize = 8 + 8 + 8 + 8 + 8 + 8 + 8 + 4 ; 
		
		
		int valueSize = 8 + 8 + 8 + 8 + 8 + 8 + 8 + 4 + 8 + 8 + 8 ;
		try (ChronicleMap<Long, PostalCodeRangeImpl> map = ChronicleMapBuilder.of(Long.class, PostalCodeRangeImpl.class)
				// .valueSize(valueSize)
				.averageValueSize(valueSize)
				.entries(ENTRIES)
				// .createOrRecoverPersistedTo(file.toFile())
				.createPersistedTo(file.toFile())) {
			long start = System.currentTimeMillis();
			PostalCodeRangeImpl value = new PostalCodeRangeImpl();

			long entryPut = 0;
			for (long i = 0; i < ENTRIES; i++) {
				
				
				if ( i % SHOW_INTERVAL_ITERATIONS == 0) {
					System.out.printf("put %d keys%n", i);
					
					

				}
				if (i == 1234567) {
					
//					if (true) throw new InterruptedException() ;
					System.out.printf("special put %d keys%n", i);
//					TimeUnit.SECONDS.sleep(100);
					
					System.out.print("Press enter to continue ......");
					try {
						System.in.read();
					} catch (Exception e) {
						e.printStackTrace();
					} ;
				}
				
				
				value.setMinCode(i);
				value.setMaxCode(i * i);
				map.put(i, value);

				entryPut++;
			}

			// for (long i = 0; i < entries; i += 2000000) {
			// System.out.printf("put %d keys%n", i);
			// for (long j = i; j < i + 2000000; j++) {
			// value.setMinCode(j);
			// value.setMaxCode(j * j);
			// map.put(j, value);
			//
			// entryPut ++ ;
			// }
			// }

			long time = System.currentTimeMillis() - start;
			System.out.printf("Took %.1f second to write %,d entries%n", time / 1e3, map.longSize());

			long count = 0;
			// int attempts = 500_000;
			int attempts = ENTRIES;

			// LongStream longStream = LongStream.generate(() -> current().nextLong(1,
			// entries)).limit(attempts);
			// LongStream longStream = LongStream.range(1, attempts);
			// long[] longs = longStream.toArray();
			// System.out.println(longs.length);
			// Stopwatch timer = Stopwatch.createStarted();
			// Arrays.stream(longs).
			// parallel().
			// forEach((i) -> {
			// if (i % 10_000 == 0) {
			// System.out.println(i + ": " + map.get(i).maxCode);
			// } else {
			//
			// }
			//
			// });
			// System.out.println("Reading time " +
			// timer.stop().elapsed(TimeUnit.MILLISECONDS));

			// for (long i = attempts * 1; i < attempts * 2; i++) {
			// count += ThreadLocalRandom.current().nextLong(1, entries);
			// map.containsKey(ThreadLocalRandom.current().nextLong(1, entries));
			// map.get(ThreadLocalRandom.current().nextLong(1, entries));
			// System.out.println(map.get(ThreadLocalRandom.current().nextLong(1,
			// entries)).maxCode);
			// map.get(i);
			// }
			// System.out.println("count = " + count);

			System.out.println("Map size:" + map.size());
			System.out.println("entryPut size:" + entryPut);

			map.close();

		}

		System.out.println("Process finished");
	}

	// private static void option2() throws IOException {
	// Path file = getFile();
	//// Files.deleteIfExists(file);
	//// int entries = 10_000_000; // works
	// int entries = 100_000_000;
	// ChronicleMapBuilder<Long, PostalCodeRangeImpl> builder =
	// ChronicleMapBuilder.of(Long.class, PostalCodeRangeImpl.class)
	//// .valueSize(56)
	// .entries(entries);
	// ChronicleMap<Long, PostalCodeRangeImpl> map = null;
	// map = builder.createPersistedTo(file.toFile());
	//
	//
	// long count = entries / 3;
	// try {
	// for (long i = 0; i < count - 5; i++) {
	// map.put(i, new PostalCodeRangeImpl(i));
	// if (i % 50000 == 0) {
	// System.out.println("i = " + i);
	// }
	// }
	// map.close();
	// map = builder.createPersistedTo(file.toFile());
	// for (long i = count; i < count * 2 - 5; i++) {
	// map.put(i, new PostalCodeRangeImpl(i));
	// if (i % 50000 == 0) {
	// System.out.println("i = " + i);
	// }
	// }
	// map.close();
	// map = builder.createPersistedTo(file.toFile());
	// for (long i = count * 2; i < count * 3 - 5; i++) {
	// map.put(i, new PostalCodeRangeImpl(i));
	// if (i % 50000 == 0) {
	// System.out.println("i = " + i);
	// }
	// }
	//// System.gc();
	//// for (long i = count; i < 2* count; i++) {
	//// map.put(i, new PostalCodeRangeImpl(i));
	//// }
	//// System.gc();
	//// for (long i = 2*count; i < 3*count; i++) {
	//// map.put(i, new PostalCodeRangeImpl(i));
	//// }
	//// System.gc();
	//
	//
	//// for (long i = 0; i < count; i = i + 50000) {
	//// System.out.println(map.get(i));
	//// }
	// System.out.println(map.get(count - 1).minCode + "====" + map.get(count -
	// 1).maxCode);
	// System.out.println(map.longSize());
	// } finally {
	// assert map != null;
	// map.close();
	// }
	// }


	// private static void option1() throws IOException {
	//// ChronicleMap<String, IPostalCodeRangeImpl> chm =
	// OffHeapUpdatableChronicleMapBuilder
	//// .of(String.class, IPostalCodeRangeImpl.class)
	//// .keySize(10)
	//// .create();
	//
	// Path file = getFile();
	// ChronicleMapBuilder builder = ChronicleMapBuilder.of(Long.class,
	// PostalCodeRangeImpl.class).
	// entries(1_000_000_000);
	//
	// ChronicleMap<Integer, PostalCodeRangeImpl> map =
	// builder.createPersistedTo(file.toFile());
	//
	// int count = 20;
	// for (int i = 0; i < count; i++) {
	// PostalCodeRangeImpl pcr = new PostalCodeRangeImpl() ;
	// pcr.setMaxCode((int) (i * i));pcr.setMinCode((int) i);
	// map.put(Integer.valueOf(i), pcr );
	// }
	// System.out.println(map.get(count - 1));
	// System.out.println(map.longSize());
	// }

	private static Path getFile() {
		// String tempDir = System.getProperty("java.io.tmpdir");
		String tempDir = "C:/tmp";
		System.out.println(tempDir);
		return Paths.get(tempDir, "SimpleTest");
	}
	
	
//    private static Instrumentation instrumentation;
//
//    public static void premain(String args, Instrumentation inst) {
//        instrumentation = inst;
//    }
//
//    public static long getObjectSize(Object o) {
//        return instrumentation.getObjectSize(o);
//    }
}
