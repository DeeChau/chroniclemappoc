package com.hsbc;

import net.openhft.chronicle.map.ChronicleMap;
import net.openhft.chronicle.map.ChronicleMapBuilder;
// import net.openhft.lang.values.IntValue;
import net.openhft.chronicle.core.values.IntValue;
import com.hsbc.PostalCodeRangeImpl;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;

public class OffHeapReadFromFilePoc {
    private static final int ITERATIONS = 10_000_000;
    private static final int MEGABYTE = (1024*1024);
    private static final int SHOW_INTERVAL_ITERATIONS = 1_000_000;
    
	public static void main(String[] args) throws IOException {
	

		System.out.println("----- CHRONICLE MAP ------------------------");
		
		File file = new File("C:/tmp/SimpleTest");
		
//		File file = new File("D:/tmp/chronicle-map.map") ;
//		File file = new File("D:/tmp/chronicle-map-read-2.map");
//		file.deleteOnExit();

		ChronicleMapBuilder<Long, PostalCodeRangeImpl> builder = ChronicleMapBuilder
				.of(Long.class, PostalCodeRangeImpl.class).entries(ITERATIONS);

		try (ChronicleMap<Long, PostalCodeRangeImpl> map = builder.recoverPersistedTo(file, true)) {
//			final PostalCodeRangeImpl value = map.newValueInstance();
//			final IntValue key = map.newKeyInstance();
			long actualQuantity = 0;
			long expectedQuantity = 0;

			long time = System.currentTimeMillis();

			System.out.println("*** Entering critical section ***");
			
			System.out.println("size of map:" + map.size());
			
			TimeUnit.SECONDS.sleep(2);
			System.out.print("Press enter to continue ......");
			try {
				System.in.read();
			} catch (Exception e) {
				e.printStackTrace();
			} ;
			
			
			System.out.println("size of map [[[again]]] :" + map.size());
			 
		
			for (int i = 0; (i< map.size() && i < ITERATIONS); i++) {
				
				if (map.get((long)i) == null) {
					System.out.println(i + ": map.get is null , it should _NOT_ happen !!!" );
					continue ;
				}
//				value.setQuantity(i);
//				key.setValue(i);
//				map.put(key, value);
				expectedQuantity += i;
				
				
				if ( i % SHOW_INTERVAL_ITERATIONS == 0) {
					System.out.println("map content, at " + i + ": " + map.get((long)i).maxCode);
				}
				
				if ( i > (map.size() - 10)) {
					System.out.println("last 10 map content, at " + i + ": " + map.get((long)i).maxCode);
				}
				
			}
				

			long putTime = (System.currentTimeMillis() - time);
			time = System.currentTimeMillis();

//			for (int i = 0; (i< map.size() && i < ITERATIONS ); i++) {
////				key.setValue(i);
////				actualQuantity += map.getUsing(key, value).getQuantity();
//			}

			System.out.println("*** Exiting critical section ***");

			System.out.println("Time execution: " + putTime);
//			System.out.println("Time for getting " + (System.currentTimeMillis() - time));

//			Assert.assertEquals(expectedQuantity, actualQuantity);
			printMemUsage();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		
			file.delete();
		}
	}
	
	
    public static void printMemUsage(){
        System.gc();
        System.gc();
        System.out.println("Memory(heap) used " + humanReadableByteCount(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory(), true));
    }
    
    public static String humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }


}
