

package com.hsbc;



import net.openhft.chronicle.bytes.Byteable;
import net.openhft.chronicle.bytes.BytesMarshallable;
import net.openhft.chronicle.map.* ;
import net.openhft.chronicle.values.Copyable;


public interface PostalCodeRange extends java.io.Serializable {
	
    long getMinCode();
    void setMinCode(long minCode);

    long getMaxCode();
    void setMaxCode(long maxCode);

}
