package com.hsbc;

import net.openhft.chronicle.map.ChronicleMap;
import net.openhft.chronicle.map.ChronicleMapBuilder;
// import net.openhft.lang.values.IntValue;
import net.openhft.chronicle.core.values.IntValue;
import com.hsbc.BondVOInterface;

import java.io.File;
import java.io.IOException;

import org.junit.Assert;

public class OffHeap_Obsoleted_WriteToFile {
//    private static final int ITERATIONS = 10_000_000;
	private static final int ITERATIONS = 10_000;
    private static final int MEGABYTE = (1024*1024);
    
	public static void main(String[] args) throws IOException, InterruptedException {
		
		System.out.println("not this program, use aonther class to do test writing, probably OffHeapTestWriter") ;
		System.exit(1);
	

		System.out.println("----- CHRONICLE MAP ------------------------");
		File file = new File("D:/tmp/chronicle-map.map");
		// file.deleteOnExit();

		ChronicleMapBuilder<Integer, BondVOImpl> builder = ChronicleMapBuilder
				.of(Integer.class, BondVOImpl.class).entries(ITERATIONS);
		
		// .of(Integer.class, BondVOInterface.class).averageValueSize(30_000).entries(ITERATIONS);

		try (ChronicleMap<Integer, BondVOImpl> map = builder.createOrRecoverPersistedTo(file, false)) {
//			final BondVOInterface value = map.newValueInstance();
//			final IntValue key = map.newKeyInstance();
//			final BondVOImpl value = new BondVOImpl();
			final BondVOImpl valueForRead = new BondVOImpl();
			Integer key ;
			long actualQuantity = 0;
			long expectedQuantity = 0;

			long time = System.currentTimeMillis();

			System.out.println("*** Entering critical section ***");

			for (int i = 0; i < ITERATIONS; i++) {
				BondVOImpl value = new BondVOImpl();
				value.setQuantity(i);
				value.setId(i * 2);
				value.setIssueDate(System.currentTimeMillis());
				key = i;
				map.put(key, value);
				expectedQuantity += i;
				
				// System.out.println("1st loop, key: " + key + " \t expectedQuantity:" + expectedQuantity + " \tmap size: " + map.longSize());
			}

			long putTime = (System.currentTimeMillis() - time);
			time = System.currentTimeMillis();

			for (int i = 0; i < ITERATIONS; i++) {
				key = i;
				actualQuantity += ((BondVOImpl)map.getUsing(key, valueForRead)).getQuantity();
				
				System.out.println("key: " + key + " \t acutalQuntity:" + actualQuantity);
			}

			System.out.println("*** Exiting critical section ***");

			System.out.println("Time for putting " + putTime);
			System.out.println("Time for getting " + (System.currentTimeMillis() - time));

			Assert.assertEquals(expectedQuantity, actualQuantity);
			printMemUsage();
			
			Thread.sleep(5000000) ;

		} catch (Exception e) {
			
			e.printStackTrace();
			
		} finally {
		
			// file.delete();
		}
	}
	
	
    public static void printMemUsage(){
        System.gc();
        System.gc();
        System.out.println("Memory(heap) used " + humanReadableByteCount(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory(), true));
    }
    
    public static String humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }


}
