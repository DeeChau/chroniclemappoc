package com.hsbc;

public class PostalCodeRangeImpl implements PostalCodeRange {

	long minCode, maxCode;
	
	public PostalCodeRangeImpl (long lg) {
		this.minCode = lg ;
		this.maxCode = (lg * lg);
	}
	public PostalCodeRangeImpl () {
		
	}
	
	@Override
	public long getMinCode() {
		// TODO Auto-generated method stub
		return this.minCode;
	}

	@Override
	public void setMinCode(long j) {
		// TODO Auto-generated method stub
		this.minCode = j;

	}

	@Override
	public long getMaxCode() {
		// TODO Auto-generated method stub
		return this.maxCode;
	}

	@Override
	public void setMaxCode(long maxCode) {
		// TODO Auto-generated method stub
		this.maxCode = maxCode ;

	}

}
